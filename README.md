Pyrate
======

Optical Design with Python.

FreeCAD workbench
=================

- copy (or symlink) directory PyrateWorkbench into ~/.FreeCAD/Mod
- execute ./build_rc in PyrateWorkbench directory
- choose workbench in FreeCAD
- create "core" symlink via "ln -s ../ core" to base modules of pyrate within PyrateWorkbench directory (is there a better possibility to import core modules in a PEP 8 conform manner?)
